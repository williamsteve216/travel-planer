/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        gray: {
          100: '#626F7C',
          200: '#1D2F41',
          300: '#ecf0f4',
          400: '#d7dde2'
        }
      }
    }
  },
  plugins: []
}
