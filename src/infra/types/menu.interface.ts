export interface IMenu {
  items?: IMenuItem[]
  direction?: Direction
  divided?: boolean
}

export interface IMenuItem {
  label?: string
  active?: boolean
  nodes?: IMenuItem[]
}

export enum Direction {
  HORIZONTAL = 'HORIZONTAL',
  VERTICAL = 'VERTICAL'
}
