import type { Meta, StoryObj } from '@storybook/vue3'
import ServiceCard from './ServiceCard.vue'

const meta = {
  title: 'UI/Cards/ServiceCard',
  tags: ['autodocs'],
  component: ServiceCard,
  argTypes: {},
  args: {}
} satisfies Meta<typeof ServiceCard>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {}
}
