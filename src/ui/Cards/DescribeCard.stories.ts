import type { Meta, StoryObj } from '@storybook/vue3'
import DescribeCard from './DescribeCard.vue'

const meta = {
  title: 'UI/Cards/DescribeCard',
  tags: ['autodocs'],
  component: DescribeCard,
  argTypes: {},
  args: {}
} satisfies Meta<typeof DescribeCard>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {}
}
