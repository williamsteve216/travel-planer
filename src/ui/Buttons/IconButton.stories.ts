import type { Meta, StoryObj } from '@storybook/vue3'
import IconButton from './IconButton.vue'

const meta = {
  title: 'UI/Buttons/IconButton',
  tags: ['autodocs'],
  component: IconButton,
  argTypes: {
    size: { control: 'select', options: ['small', 'medium', 'large'] },
    color: { control: 'color' },
    colorHover: { control: 'color' },
    background: { control: 'color' },
    outlined: { control: 'boolean' }
  },
  args: {
    color: '#000',
    colorHover: 'red'
  }
} satisfies Meta<typeof IconButton>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {}
}

export const Outlined: Story = {
  args: {
    outlined: true
  }
}
