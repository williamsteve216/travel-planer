import type { Meta, StoryObj } from '@storybook/vue3'
import ButtonMore from './ButtonMore.vue'

const meta = {
  title: 'UI/Buttons/ButtonMore',
  tags: ['autodocs'],
  component: ButtonMore,
  argTypes: {},
  args: {}
} satisfies Meta<typeof ButtonMore>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {}
}
