import type { Meta, StoryObj } from '@storybook/vue3'
import ButtonSlide from './ButtonSlide.vue'

const meta = {
  title: 'UI/Buttons/ButtonSlide',
  tags: ['autodocs'],
  component: ButtonSlide,
  argTypes: {
    size: { control: 'select', options: ['small', 'medium', 'large'] }
  },
  args: { primary: true } // default value
} satisfies Meta<typeof ButtonSlide>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {
    label: 'Book a consultation'
  }
}
