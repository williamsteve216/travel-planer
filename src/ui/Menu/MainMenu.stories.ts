import type { Meta, StoryObj } from '@storybook/vue3'

import MainMenu from './MainMenu.vue'

const meta = {
  title: 'UI/Menu/MainMenu',
  tags: ['autodocs'],
  component: MainMenu,
  argTypes: {
    background: { control: 'color' },
    color: { control: 'color' },
    colorHover: { control: 'color' },
    colorBarActive: { control: 'color' }
  },
  args: {
    background: '#1D2F41',
    color: '#AAB3BB',
    colorHover: '#ffffff',
    colorBarActive: '#ff494a'
  }
} satisfies Meta<typeof MainMenu>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {}
