import type { Meta, StoryObj } from '@storybook/vue3'
import Typography from './Typography.vue'

const meta = {
  title: 'UI/Typography',
  tags: ['autodocs'],
  component: Typography,
  argTypes: {
    color: { control: 'color' },
    colorHover: { control: 'color' }
  },
  args: {
    color: '#000',
    colorHover: 'red',
    label: 'Votre texte ici'
  }
} satisfies Meta<typeof Typography>

export default meta

type Story = StoryObj<typeof meta>

export const Default: Story = {
  args: {}
}
